<?php 
/*
Plugin Name: Admin Default Page
Description: Change default page per user after login
Plugin URI: http://facebook.com/ozamorowski
Author: Oskar Zamorowski
Version: 1.0
License: GPL2
*/

/*

    Copyright (C) 2016  Oskar Zamorowski  ozamorowski@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


/* Redirect the user logging in to a custom admin page. */
function stch_admin_default_page($redirect_to, $request, $user){

    $user_default_page = get_the_author_meta( 'default_admin_page' );

    // print_r( admin_url( $user_default_page ));

    return admin_url( $user_default_page );

}
add_action('login_redirect', 'stch_admin_default_page', 10, 3);


/**
 * Create input on user profile page.
 */
add_action( 'show_user_profile', 'stch_extra_profile_fields' );
add_action( 'edit_user_profile', 'stch_extra_profile_fields' );

function stch_extra_profile_fields( $user ) { ?>

    <h3>Default page after login</h3>

    <table class="form-table">

        <tr>
            <th><label for="default_admin_page"><?php echo admin_url(); ?></label></th>

            <td>
                <input type="text" name="default_admin_page" id="default_admin_page" value="<?php echo esc_attr( get_the_author_meta( 'default_admin_page', $user->ID ) ); ?>" class="regular-text" /><br />
                <p class="description">Enter addres. Ex. "edit.php"</p>
                <p class="description"><?php echo get_the_author_meta( 'default_admin_page', $user->ID ); ?></p>
            </td>
        </tr>

    </table>
<?php }

/**
 * Update usermeta with default page.
 */
add_action( 'personal_options_update', 'stch_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'stch_save_extra_profile_fields' );

function stch_save_extra_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;

    update_user_meta( $user_id, 'default_admin_page', $_POST['default_admin_page'] );
}

 ?>